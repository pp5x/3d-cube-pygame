#!/usr/bin/python
## display.py for 3D Project
##
## Made by Pierre Pagnoux
## <Pierre.Pagnoux@gmail.com>
##
## Started on  Wed Jun 26 20:08:34 2013 Pierre Pagnoux
## Last update Sat Jun 29 12:44:26 2013 Pierre Pagnoux
##

from OpenGL.GL import *
from OpenGL.GLU import *
from pygame.locals import *
import pygame

class Cube(object):
    def __init__(self):
        self.vector = (1, 1, 0.5)
        self.speed = 1

    def draw(self):
        glBegin(GL_QUADS)
        glColor(255, 0, 0)
        glVertex( 1,  1, -1)
        glVertex(-1,  1, -1)
        glVertex(-1, -1, -1)
        glVertex( 1, -1, -1)
        glEnd()

        glBegin(GL_QUADS)
        glColor(0, 255, 0)
        glVertex( 1,  1, -1)
        glVertex( 1,  1,  1)
        glVertex(-1,  1,  1)
        glVertex(-1,  1, -1)
        glEnd()

        glBegin(GL_QUADS)
        glColor(0, 0, 255)
        glVertex(-1,  1, -1)
        glVertex(-1,  1,  1)
        glVertex(-1, -1,  1)
        glVertex(-1, -1, -1)
        glEnd()

        glBegin(GL_QUADS)
        glColor(255, 255, 0)
        glVertex(-1, -1, -1)
        glVertex(-1, -1,  1)
        glVertex( 1, -1,  1)
        glVertex( 1, -1, -1)
        glEnd()

        glBegin(GL_QUADS)
        glColor(255, 0, 255)
        glVertex( 1, -1, -1)
        glVertex( 1, -1,  1)
        glVertex( 1,  1,  1)
        glVertex( 1,  1, -1)
        glEnd()

        glBegin(GL_QUADS)
        glColor(0, 255, 255)
        glVertex( 1,  1,  1)
        glVertex(-1,  1,  1)
        glVertex(-1, -1,  1)
        glVertex( 1, -1,  1)
        glEnd()

        glFlush()

    def rotate(self):
        glRotate(self.speed, *self.vector)

class Camera(object):
    def __init__(self, size, fovy, near, far):
        self.ratio = size[0]/size[1]
        self.fovy = fovy
        self.near = near
        self.far = far

    def update(self):
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(self.fovy, self.ratio, self.near, self.far)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        gluLookAt(3, 3, 3, 0, 0, 0, 0, 0, 1)

class Master(object):
    def __init__(self):
        self.size = (640, 480)
        self.flags = OPENGL|DOUBLEBUF|HWSURFACE
        self.clear_color = (0.1, 0.1, 0.1, 1)
        self.stop = False
        self.clock = pygame.time.Clock()
        self.camera = Camera(self.size, 70, 1, 10)
        self.cube = Cube()

    def start(self):
        self.init()
        self.initGL()
        self.run()
        self.finish()

    def init(self):
        pygame.init()
        self.screen = pygame.display.set_mode(self.size, self.flags)

    def initGL(self):
        glViewport(0, 0, 640, 480)
        glClearColor(*self.clear_color)
        glEnable(GL_DEPTH_TEST)
        self.camera.update()

    def clear_screen(self):
        glClear(GL_DEPTH_BUFFER_BIT|GL_COLOR_BUFFER_BIT)

    def update_screen(self):
        pygame.display.flip()

    def sync(self):
        glFinish()

    def handle_events(self, events):
        for event in events:
            if event.type == QUIT:
                self.end()

    def handle_keys(self, keys):
        if keys[K_ESCAPE]:
            self.end()

    def draw(self):
        self.cube.draw()
        self.cube.rotate()

    def end(self):
        self.stop = True

    def finish(self):
        print self.clock.get_fps()
        pygame.quit()

    def run(self):
        while not self.stop:
            self.clock.tick(50)
            self.handle_events(pygame.event.get())
            self.handle_keys(pygame.key.get_pressed())
            self.clear_screen()
            self.draw()
            self.update_screen()
            self.sync()

if __name__ == '__main__':
    application = Master()
    application.start()
