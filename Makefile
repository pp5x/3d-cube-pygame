##
## Makefile for 3D Project
## 
## Made by Pierre Pagnoux
## <Pierre.Pagnoux@gmail.com>
## 
## Started on  Wed Jun 26 20:01:40 2013 Pierre Pagnoux
## Last update Wed Jun 26 22:45:11 2013 Pierre Pagnoux
##

MAIN = main.py

run:
	python main.py

clean:
	@rm -rf *~ *#
